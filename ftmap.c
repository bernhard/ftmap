#include <stdio.h>
#include <stdlib.h>
#include <avro.h>

// output field separator
#define FS ','

/**
 * Outputs certain fields from each record.
 *
 * The fields are separated by FS, records are separated by a newline.
 * The exact fields to extract are defined in the while-loop.
 * These probably have to be changed for the task at hand.
 */
int print_records(const avro_file_reader_t db, const avro_schema_t schema)
{
    avro_value_t avro_long, avro_string, avro_int, avro_array;
    int32_t int_lit;
    int64_t long_lit;
    const char* string_lit;
    size_t len;

    avro_value_iface_t* record_class = avro_generic_class_from_schema(schema);
    if (record_class == NULL) {
        return 1;
    }

    avro_value_t record;
    avro_generic_value_new(record_class, &record);
    if (record_class == NULL) {
        return 1;
    }

    while (avro_file_reader_read_value(db, &record) == 0) {
        // print a long value
        if (avro_value_get_by_name(&record, "src_ip", &avro_long, NULL) == 0) {
            avro_value_get_long(&avro_long, &long_lit);
            fprintf(stdout, "%"PRId64, long_lit);
        }
        fputc(FS, stdout);

        // print an int value
        if (avro_value_get_by_name(&record, "dst_port", &avro_int, NULL) == 0) {
            avro_value_get_int(&avro_int, &int_lit);
            fprintf(stdout, "%"PRId32, int_lit);
        }
        fputc(FS, stdout);

        // print a string value
        if (avro_value_get_by_name(&record, "netacq_country", &avro_string, NULL) == 0) {
            avro_value_get_string(&avro_string, &string_lit, &len);
            fputs(string_lit, stdout);
        }
        fputc(FS, stdout);

        // print variable-sized array of long values
        if (avro_value_get_by_name(&record, "common_pktsizes", &avro_array, NULL) == 0) {
            avro_value_get_size(&avro_array, &len);

            fputc('[', stdout);
            for (unsigned int i = 0; i < len; i++) {
                avro_value_get_by_index(&avro_array, i, &avro_long, NULL);
                avro_value_get_long(&avro_long, &long_lit);
                fprintf(stdout, "%"PRId64, long_lit);

                if (i != len - 1) {
                    fputc(FS, stdout);
                }
            }
            fputc(']', stdout);
        }

        fputc('\n', stdout);
    }

    // avro_long, avro_string, avro_int, avro_array must be not be decref'd
    avro_value_decref(&record);
    avro_value_iface_decref(record_class);

    return 0;
}

/**
 * Entry point.
 */
int main(int argc, char* argv[])
{
    if (argc > 2) {
        fprintf(stderr,
                "Usage: %s [avro_file]\n"
                "Without arguments standard input is used\n",
                argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* dbname;
    if (argc == 2) {
        dbname = argv[1];
    } else {
        dbname = "/dev/stdin";
    }

    avro_file_reader_t dbreader;

    if (avro_file_reader(dbname, &dbreader)) {
        fprintf(stderr, "Error opening file: %s\n", avro_strerror());
        exit(EXIT_FAILURE);
    }

    avro_schema_t schema = avro_file_reader_get_writer_schema(dbreader);

    if (print_records(dbreader, schema)) {
        fprintf(stderr, "Error printing records: %s\n", avro_strerror());
        exit(EXIT_FAILURE);
    }

    avro_file_reader_close(dbreader);
    avro_schema_decref(schema);

    return EXIT_SUCCESS;
}
