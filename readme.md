# ftmap

A simple tool for extracting a fixed set of columns from [Stardust data files](https://github.com/CAIDA/pyavro-stardust/wiki/Supported-Data-Formats).

Each record is converted to a comma-separated row and is written to standard output.
By default, it reads flowtuple v4 files and outputs the `src_ip`, `dst_port`, `netacq_country`, and `common_pktsizes` fields.
Refer to the [Avro C documentation](https://avro.apache.org/docs/current/api/c/index.html) for reading other data types.

## Installation

Ensure libavro and its headers are installed and `make`.

## Example

```sh
ft='swift://telescope-ucsdnt-avro-flowtuple-v4-2021/datasource=ucsd-nt/year=2021/month=05/day=22/hour=22/v3_5min_1621723500.ft4.avro'
wandiocat $ft | ./ftmap >mapped_values.csv
```
